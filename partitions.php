<?php
/*Порождение всех разбиений.   Generation of all partitions.*/
$a = array(
	1,
	1,
	1,
	1,
	1,
	1,
	1
);
$min_elem=0;
$n = count($a);

while (1)
	{
	/*Печать и выход. Print end exit.*/
	print_r($a);
	if ($a[0] == $n) break;
	
	/*Элемент в нулевом индексе нашего динамического
	массива на текущий момент.
	First element of our dynamic array*/
	$first_elem = $a[0];

	$i = 0;
	while ($i != count($a) - 1)
		{
		/*Найдем элемент меньше первого. Here we search min. element.*/
		if ($a[$i] < $first_elem)
			{
			$first_elem = $a[$i];
			$min_elem = $i;
			}

		$i++;
		}
	
	/*Перенос элемента  "1". Here we transfer "1". */
	$a[$min_elem]+= 1;
	$a[count($a) - 1]-= 1;
	
	/*Обрежем массив и найдем сумму его элементов. We cut the array
	* and count the sum of elements.*/
	array_splice($a, $min_elem + 1);
	$array_sum = array_sum($a);
	
	/*Добавим в массив единицы заново с учетом суммы.
	Here we add 1 (fill)  to the array
	( taking into account the sum ).*/
	for ($j = 0; $j != $n - $array_sum; $j++) $a[] = 1;
	
	/*Обнулим переменную. Unset min_elem. You can use unset($min_elem) instead of a string below*/
	$min_elem=0;
	}

?>
