<?php
/*Порождение всех разбиений для композиций.   Generation  of all partitions for compositions.*/
$a = array(
	1,
	1,
	1,
	1,
	1
);
$n = count($a);
$min_elem=0;
$h=0;
while (1)
	{
	/*Печать всех перестановок и условие выхода.
	Permutations and exit.*/
	permute($a, $h);
	if ($a[0] == $n) break;
	/*Элемент в нулевом индексе нашего динамического
	массива на текущий момент.
	First element of our dynamic array*/
	$first_elem = $a[0];
	$i = 0;
	while ($i != count($a) - 1)
		{
		/*Найдем элемент меньше первого. Here we search min. element.*/
		if ($a[$i] < $first_elem)
			{
			$first_elem = $a[$i];
			$min_elem = $i;
			}

		$i++;
		}

	/*Перенос элемента  "1". Here we transfer "1". */
	$a[$min_elem]+= 1;
	$a[count($a) - 1]-= 1;
	
	/*Обрежем массив и найдем сумму его элементов. We cut the array
	* and count the sum of elements.*/
	array_splice($a, $min_elem + 1);
	$array_sum = array_sum($a);
	
	/*Добавим в массив единицы заново с учетом суммы.
	Here we add 1 (fill)  to the array
	( taking into account the sum ).*/
	for ($j = 0; $j != $n - $array_sum; $j++) $a[] = 1;
	
	/*Обнулим переменную. Unset min_elem. You can use unset($min_elem) instead of a string below*/
	$min_elem=0;

	}

/*Функция перестановок. Permutations function.*/
function permute($b,&$h)
	{
	/*Дублируем массив и перевернем его. Это нужно для выхода
	 * из алгоритма.
	 * Here we make a copy and reverse our array. This is necessary to
	 * stop the algorithm.*/
	$a = $b;
	$b = array_reverse($b);

	while (1)
		{
		
		/*Печать и условие выхода. Here we print and exit.*/
		print_r($a);
		print "\n";
		if ($a == $b) break;
		
		/*Ищем следующую перестановку.
		 * Here we search next permutation.*/
		$i = 1;
		while ($a[$i] >= $a[$i - 1])
			{
			$i++;
			}

		$j = 0;
		while ($a[$j] <= $a[$i])
			{
			$j++;
			}

		/*Обмен. Here we change.*/
		$c = $a[$j];
		$a[$j] = $a[$i];
		$a[$i] = $c;
		
		/*Обернем хвост. Tail reverse.*/
		$c = $a;
		$z = 0;
		for ($i-= 1; $i > - 1; $i--) $a[$z++] = $c[$i];

		}

	}

?>
